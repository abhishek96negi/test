# -*- coding: utf-8 -*-
{
    'name': "Online Academy",

    'summary': """
        Odoo 14 Development Tutorial""",

    'description': """
        Odoo 14 Development Tutorial,
        Development Tutorial,
        Odoo Tutorial,
        Odoo14, odoo Tutorials, odoo learning, odoo13 Tutorials, odoo14 Tutorials, Tutorial,
        Open acadaemy module for managing trainings:
        -Manage student enroll
        -Attendance registration
        -Trainer
        -Training sessions
        -Courses
        -More..
    """,

    'author': 'Abhishek Negi',
    'company': 'xyz',
    'website': 'https://www.xyz.com',
    'live_test_url': 'https://www.xyz.com',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Tutorials',
    'version': '14.0.1.4.0',
    'license': 'AGPL-3',

    # any module necessary for this one to work correctly
    'depends': ['base', 'board', 'website_slides'],
    # always loaded
    'data': [
       # 'data/slide_channel_data.xml',
       # 'data/slide_channel_data_v13.xml',
       # 'security/security.xml',
        #'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/openacademy.xml',
        'views/partner.xml',
        #'views/session_board.xml',
        #'reports.xml',
        'demo/demo.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
