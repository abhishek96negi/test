from odoo import models, fields, api


class Wizard(models.TransientModel):
    _name = 'bugfix_new.wizard'
    _description = "Wizard: Changing the status of the state"

    @api.model
    def _change_state(self):
        return self.env['bugfix_new.issue'].browse(self._context.get('active_ids'))

    issue_name = fields.Many2one("bugfix_new.issue", string="Issuer", required=True, domain="[('state', '=', 'Open')]")
    state = fields.Selection([('Open', 'Open'), ('Close', 'Close')], default="Open", readonly=True)
   # state = fields.Many2one('bugfix_new.issue', string="State", required=True, default=_change_state)

    def change_status(self):
     #   contract = self.env['bugfix_new.issue'].browse(self.env.context.get('active_ids'))
    #    print(contract)
   #     print(contract.state)
      #print(self.issue_name.state)
        if self.issue_name.state == 'Open':
            self.issue_name.state = 'Close'
