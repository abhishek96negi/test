# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning


class Issue(models.Model):
    _name = 'bugfix_new.issue'
    _description = 'BugFix New Issues Description'
    _rec_name = 'issue_name'

   # @api.onchange('state')
   # def change_state(self):
     #   for rec in self:
     #       return {'domain': {'state': [{'state', '=', 'Open'}]}}

    issue_name = fields.Char(string="Issue Name", required=True)
    state = fields.Selection([('Open', 'Open'), ('Close', 'Close')], default="Open", readonly=True)
 #   state = fields.Char(string="State", default="Open", readonly=True)
    responsible_id = fields.One2many("bugfix_new.resolve", 'issue_name', string="Resolve")

    def state_change(self):
        if self.responsible_id.login:
            if self.state == 'Open':
                self.state = 'Close'
                return True
        else:
            message = {
                'type': 'ir.actions.client',
                'tag': 'display_notification',
                'params': {
                    'title': 'Warning!',
                    'message': 'There is no comment ',
                    'sticky': False,
                }
            }
            return message



class Resolve(models.Model):
    _name = 'bugfix_new.resolve'
    _description = 'Resolve Issues Description'
    _rec_name = 'issue_name'

    issue_name = fields.Many2one("bugfix_new.issue", string="Issuer", ondelete="cascade", required=True, domain="[('state', '=', 'Open')]")
    comment = fields.Text(string="Comment")
    latest_authentication = fields.Datetime(string="Latest Authentication", default=fields.Datetime.now(), readonly=True)
    login = fields.Char(string="Login", default=lambda self: self.env.user.email, readonly=True)
    # color = fields.Integer()
## user, uid ---- Name of the current user

## id, name,email ---- error

# login = fields.Many2one('res.users', 'login', default = lambda self: self.env.uid.email, readonly=True)
 #   login = fields.Char(default=lambda self: self.env.email)