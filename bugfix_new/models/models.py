# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Issue(models.Model):
    _name = 'bugfix_new.issue'
    _description = 'BugFix New Issues Description'
    _rec_name = 'issue_name'

    issue_name = fields.Char(string="Issue Name", required=True)
  #  comment = fields.Char(string="Comment")
 #   login = fields.Char(string="Login")
#    latest_authentication = fields.Date(string="Latest Authentication", default=fields.Date.today())

    responsible_id = fields.One2many("bugfix_new.resolve", 'issue_name', string="Resolve")


class Resolve(models.Model):
    _name = 'bugfix_new.resolve'
    _description = 'Resolve Issues Description'
    _rec_name = 'issue_name'

    issue_name = fields.Many2one("bugfix_new.issue", string="Issuer", ondelete="cascade", required=True)
    comment = fields.Text(string="Comment")
    latest_authentication = fields.Datetime(string="Latest Authentication", default=fields.Datetime.now(), readonly=True)
  #  login = fields.Many2one('res.users', 'login', default=lambda self: self.env.user, readonly=True)
    login = fields.Char(string="Login", default=lambda self: self.env.user.email, readonly=True)
## user, uid ---- Name of the current user

## id, name,email ---- error

# login = fields.Many2one('res.users', 'login', default = lambda self: self.env.uid.email, readonly=True)
 #   login = fields.Char(default=lambda self: self.env.email)